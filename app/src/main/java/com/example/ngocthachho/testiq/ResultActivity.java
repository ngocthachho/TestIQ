package com.example.ngocthachho.testiq;

/**
 * Created by ngocthachho on 7/13/2016.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.TextView;

public class ResultActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        TextView textResult = (TextView) findViewById(R.id.textResult);

        Bundle b = getIntent().getExtras();

        int score = b.getInt("score");

        textResult.setText("Your IQ is" + " " + (72+score*5)+ ". Thank you!!");

    }

    public void playagain(View o) {

        Intent intent = new Intent(this, QuestionActivity.class);

        startActivity(intent);
    }
    public void backhome (View o){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);

    }

}
