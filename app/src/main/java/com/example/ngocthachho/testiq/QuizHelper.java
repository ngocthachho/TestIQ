package com.example.ngocthachho.testiq;

/**
 * Created by ngocthachho on 7/13/2016.
 */

import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
public class QuizHelper extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "mathsone";
    // tasks table name
    private static final String TABLE_QUEST = "quest";
    // tasks Table Columns names
    private static final String KEY_ID = "qid";
    private static final String KEY_QUES = "question";
    private static final String KEY_ANSWER = "answer";
    private static final String KEY_OPTA = "opta";
    private static final String KEY_OPTB = "optb";
    private static final String KEY_OPTC = "optc";

    private SQLiteDatabase dbase;

    public QuizHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        dbase = db;
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_QUEST + " ( "
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_QUES
                + " TEXT, " + KEY_ANSWER + " TEXT, " + KEY_OPTA + " TEXT, "
                + KEY_OPTB + " TEXT, " + KEY_OPTC + " TEXT)";
        db.execSQL(sql);
        addQuestion();

    }

    private void addQuestion() {
        Question q1 = new Question("1, 23, 44, 5, 67, ?", "88", "62", "96", "88");
        this.addQuestion(q1);
        Question q2 = new Question("X, 6, Y, ?", "5", "z", "7", "7");
        this.addQuestion(q2);
        Question q3 = new Question("3, ?, 3, 3, 6, 9, 15", "3", "0", "6", "0");
        this.addQuestion(q3);
        Question q4 = new Question("A, B, D, H, AF, CB, ?", "ca", "de", "fd", "fd");
        this.addQuestion(q4);
        Question q5 = new Question("?, 5, 5, 0, 1, 0, 0, 2, 0, 0, 4, 0, 0", "1", "2", "4", "2");
        this.addQuestion(q5);
        Question q6 = new Question("6, 21, 42, 84, 69, 291, ?", "427", "459", "483", "483");
        this.addQuestion(q6);
        Question q7 = new Question("xyz, z, ab, ?, n, opp", "aa", "bb", "cc", "cc");
        this.addQuestion(q7);
        Question q8 = new Question("3, 15, 2, 7, 45, ?", "4", "14", "24", "14");
        this.addQuestion(q8);
        Question q9 = new Question("3, ?, 12, -24", "-6", "-9", "-12", "-6");
        this.addQuestion(q9);
        Question q10 = new Question("01, ?, 56, 987", "321", "432", "543", "432");
        this.addQuestion(q10);
        Question q11 = new Question("30, 28, 84, 10, 42, 12, 5, 24, 41, ?", "31", "51", "71", "51");
        this.addQuestion(q11);
        Question q12 = new Question("21/311 , 33/6333 , 27/977 , 15/65 , 31/?", "4111", "1114", "4441", "4111");
        this.addQuestion(q12);
        Question q13 = new Question("wrong number 2 - 3 - 6 - 7 - 8 - 14 - 15", "6", "7", "8", "8");
        this.addQuestion(q13);
        Question q14 = new Question("1/4 of 1/2 of 1/5 of 200", "2", "4", "5", "5");
        this.addQuestion(q14);
        Question q15 = new Question("1 - 8 - 27 - ? - 125 - 216", "42", "64", "86", "64");
        this.addQuestion(q15);
        Question q16 = new Question("A - D - G - J - ?", "L", "M", "N", "M");
        this.addQuestion(q16);
        Question q17 = new Question("7 + 7 : 7 + 7 x 7 - 7", "35", "50", "65", "50");
        this.addQuestion(q17);
        Question q18 = new Question("+ x + - : - X + X : ? :", "x", "+", "-", "-");
        this.addQuestion(q18);
        Question q19 = new Question("DOG - MOUSE - LION - SNAKE - ELEPHANT", "MOUSE", "SNAKE", "ELEPHANT", "SNAKE");
        this.addQuestion(q19);
        Question q20 = new Question("Q - W - E - ? - T - Y", "U", "R", "T", "R");
        this.addQuestion(q20);
        // END
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUEST);

        onCreate(db);
    }


    public void addQuestion(Question quest) {

        ContentValues values = new ContentValues();
        values.put(KEY_QUES, quest.getQUESTION());
        values.put(KEY_ANSWER, quest.getANSWER());
        values.put(KEY_OPTA, quest.getOPTA());
        values.put(KEY_OPTB, quest.getOPTB());
        values.put(KEY_OPTC, quest.getOPTC());


        dbase.insert(TABLE_QUEST, null, values);
    }

    public List<Question> getAllQuestions() {
        List<Question> quesList = new ArrayList<Question>();

        String selectQuery = "SELECT  * FROM " + TABLE_QUEST;
        dbase = this.getReadableDatabase();
        Cursor cursor = dbase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Question quest = new Question();
                quest.setID(cursor.getInt(0));
                quest.setQUESTION(cursor.getString(1));
                quest.setANSWER(cursor.getString(2));
                quest.setOPTA(cursor.getString(3));
                quest.setOPTB(cursor.getString(4));
                quest.setOPTC(cursor.getString(5));

                quesList.add(quest);
            } while (cursor.moveToNext());
        }

        return quesList;
    }


}
