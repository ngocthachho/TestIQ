package com.example.ngocthachho.testiq;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;


public class HomeActivity extends AppCompatActivity {

    private Button startBtn;
    private Button exitBtn;
    private Button optionBtn;
    public CallbackManager callbackManager;
    ShareDialog shareDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.home);
        callbackManager = CallbackManager.Factory.create();

        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("https://www.facebook.com/TEST-IQ-2027719200786433"))
                .setContentTitle("WELCOME  TEST IQ!!!")
                .setContentDescription(
                        "Test IQ đi nào các bạn ơi ^^!!")
                .build();

        ShareButton shareButton = (ShareButton)findViewById(R.id.share_button);
        shareButton.setShareContent(content);

        shareDialog = new ShareDialog(this);

        startBtn = (Button) findViewById(R.id.begin);
        final Intent intent  = new Intent(this,QuestionActivity.class);
        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(intent);

            }
        });

        optionBtn =(Button) findViewById(R.id.option);
        final  Intent option = new Intent(this,Option.class);
        optionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(option);

            }
        });
        exitBtn =(Button) findViewById(R.id.exit);
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* finish();
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);*/
                finish();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });

    }

}
