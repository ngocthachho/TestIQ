package com.example.ngocthachho.testiq;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

/**
 * Created by ngocthachho on 7/13/2016.
 */
public class Option extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.option);
    }
    // Method này được gọi khi người dùng Click vào nút Start.
    public void playSong(View view)  {
        // Tạo ra một đối tượng Intent cho một dịch vụ (PlaySongService).
        Intent myIntent = new Intent(Option.this, PlaySongService.class);
        // Gọi phương thức startService (Truyền vào đối tượng Intent)
        this.startService(myIntent);
    }

    // Method này được gọi khi người dùng Click vào nút Stop.
    public void stopSong(View view)  {
        // Tạo ra một đối tượng Intent.
        Intent myIntent = new Intent(Option.this, PlaySongService.class);
        this.stopService(myIntent);
    }

    public void backhome (View o){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);

    }
}
